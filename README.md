# Task Tracker
This is repository contains a task tracker application built using MEAN stack architecture, through which we can track time taken to complete tasks.
This application is build using Angular 5 and authentication is based on JSON Web Tokens

This has both frontend and backend code.

## Front End - Angular 5 Web Application
### Features 
* Task Operations
  * Add
  * Edit
  * Delete
  * Listing
* Authentication
* Responsive application with Material Design and bootstrap


### Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

### Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

### Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

### Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## Back End - Node.js Web API 

Node.js Web API server code with bare minimum API's requried for hosting an API server

### Features
* Authentication API
* Registration 
* Database 
  * MongoDB
  
### How to use - development
* Make sure you have installed node runtime in your system
* Download the code 
* Open a command prompt and go the directory where the code has been downloaded
* Type the `npm install` command to install the npm dependencies
* Type `npm start` to run the server


### Build
* To build the code use `npm build` the build will be generated in a folder called `dist` under the source code
